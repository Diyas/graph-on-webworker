import { dateArr, orderAscDate, filter } from "./filer-methods";
let arr = [];
self.addEventListener(
  "message",
  ({ data }) => {
    // eslint-disable-next-line no-unused-vars
    switch (data.type) {
      case "init":
        orderAscDate(data.data, sortedArr => {
          arr = sortedArr;
          self.postMessage({ type: data.type, data: dateArr(sortedArr) });
        });
        break;
      case "filter":
        self.postMessage({
          type: data.type,
          data: filter(arr, data.from, data.to)
        });
        break;
    }
  },
  false
);
