const monthes = [
  "Jan",
  "Feb",
  "Mch",
  "Apl",
  "May",
  "June",
  "July",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec"
];
const getData = obj => parseInt(obj.t.slice(0, 4));

const amountValue = arr =>
  arr.reduce((a, b) => parseFloat(a.v || 0) + parseFloat(b.v || 0));

const filter = (data, from, to) => {
  const dFrom = new Date(`${from}-01-01`).getTime();
  const dTo = new Date(`${to}-12-31`).getTime();
  const arr = data.filter(i => dFrom <= i.timestamp && i.timestamp <= dTo);
  const returned = [];
  if (from + 9 <= to) {
    for (let i = from; i <= to; i++) {
      const yearData = arr.filter(y => getData(y) === i);
      returned.push({
        y: i,
        v: (amountValue(yearData) / yearData.length).toFixed(3)
      });
    }
  } else {
    for (let i = from; i <= to; i++) {
      for (let j = 1; j <= 12; j++) {
        const monthData = arr.filter(
          y => y.t.slice(0, 7) === `${i}-${j <= 9 ? `0${j}` : j}`
        );
        returned.push({
          y: i,
          d: `${monthes[j - 1]} ${i}`,
          v: (amountValue(monthData) / monthData.length).toFixed(3)
        });
      }
    }
  }
  return returned;
};
const dateArr = data => {
  const arr = [];
  for (let i = getData(data[0]); i <= getData(data[data.length - 1]); i++) {
    arr.push(i);
  }
  return arr;
};
const orderAscDate = (arr, callback) => {
  arr.map(item => (item.timestamp = new Date(item.t).getTime()));
  arr.sort((a, b) => a.timestamp - b.timestamp);
  callback(arr);
};

export { filter, dateArr, orderAscDate };
