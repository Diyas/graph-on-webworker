module.exports = {
  rules: [
    {
      test: /\.worker\.js$/,
      use: { loader: "worker-loader" }
    }
  ]
};
